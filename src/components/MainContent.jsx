import Home from "./../page/Home";
import About from "./../page/About";
import MySkills from "./../page/MySkills";
import React from "react";

function MainContent() {
    return (
    <div>
        <div className="container-fluid">
            <Home />

            <About />

            <MySkills />

        </div>
    </div>
  );
}

export default MainContent;