import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import React from 'react';

function Navigasi() {
  return (
    <div>
      <Navbar bg="dark" variant="dark" fixed='top'>
        <Container fluid>
          <Navbar.Brand href="#home">Monika Gisela</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#about">About</Nav.Link>
            <Nav.Link href="#myskills">My Skills</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <br />
    </div>
  );
}

export default Navigasi;