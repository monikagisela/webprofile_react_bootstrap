import ProgressBar from 'react-bootstrap/ProgressBar';

function My_Skills_() {
    const now = 85;
    const now1 = 75;
    const now2 = 70;
    const now3 = 80;
    return (
        <div className="row bg-light text-center" id="myskills">
        <div className="row mt-5 mb-2">
            <h1>My Skills</h1>
        </div>

        <div className="row">
            <div className="col-6 p-5">
                <h3>My creative skills and experiences.</h3>
                <p>Berikut ini adalah beberapa jenis keahlian Saya bila disimpulkan dengan persentasi. Grafik tersebut juga akan semakin meningkat sebab disela-sela kesibukan, Saya juga tidak lupa untuk terus mengasah kemampuan pada bidang tersebut dengan mengikuti course. Hal itu saya lakukan demi meningkatkan kemampuan personal Saya.</p>
            </div>
            <div className="col-6 p-5">
                <div className="row">
                    <h4>HTML</h4>
                    <ProgressBar animated now={now} label={`${now}%`} />
                </div>
                <div className="row">
                    <h4>CSS</h4>
                    <ProgressBar animated now={now1} label={`${now1}%`} />
                </div>
                <div className="row">
                    <h4>Javascript</h4>
                    <ProgressBar animated now={now2} label={`${now2}%`} />
                </div>
                <div className="row">
                    <h4>MySQL</h4>
                    <ProgressBar animated now={now3} label={`${now3}%`} />
                </div>
            </div>
        </div>
    </div>
    );
}

export default My_Skills_;