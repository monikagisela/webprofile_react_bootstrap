import monik from "./../assets/monik.jpg";

function Home_() {
    return (
        <div className="row bg-light" id="home">
            <div className="col-6 text-center mt-5 mb-3 p-5">
                <h1 className="position-relative top-50 start-50 translate-middle">Welcome To My Web Profile</h1>
            </div>
            <div className="col-6 text-center mt-5 mb-3 p-5">
                <img src={monik} alt="Monika's Photo" width="300" className='fluid rounded' />
            </div>
        </div>
    );
}

export default Home_;