import MainContent from "../components/MainContent";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

function Beranda() {
    return(
        <div>
        <Navbar />
        <MainContent />
        <Footer />
        
      </div>
      
    )
}

export default Beranda