import React from "react"
import MainContent from "./components/MainContent";
import Beranda from "./page/beranda";
import Tabel_ from "./page/TabelUser";
import {BrowserRouter, Routes, Route} from 'react-router-dom';


function App() {
  return (
    <BrowserRouter>
      <Routes >
          <Route path='/' element={<Beranda/>}/>
          <Route path='/About' element={<MainContent/>}/>
          <Route path='/ListUser' element={<Tabel_/>}/>
      </Routes>
    </BrowserRouter>
    
  );
}

export default App;
